package com.simplecrud.Service.Impl;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.simplecrud.Entity.User;
import com.simplecrud.Repository.UserRepository;
import com.simplecrud.Service.IdentityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class IdentityServiceImpl implements IdentityService {

    @Autowired
    public UserRepository userRepository;

    public String createUser(User user) throws DataIntegrityViolationException {
       try {
           User newUser = new User(user);
           userRepository.save(newUser);
           return "Success";
       }catch (DataIntegrityViolationException e){
           e.printStackTrace();
           return "Duplicate Email";
       }
    }

    public Iterable<User> getAllUsers() {
        Iterable<User> usersIterable = userRepository.findAll();
        return usersIterable;
    }


    public User getUserByEmail(String email){
        return userRepository.findAllByEmail(email);
    }

    public User updateUserByEmail(User user){
        User updateUser = userRepository.findAllByEmail(user.getEmail());
        updateUser.setName(user.getName());
        return userRepository.save(updateUser);
       }
    }

