package com.simplecrud.Service;


import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.simplecrud.Entity.User;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.List;

public interface IdentityService {

    public String createUser(User user) throws DataIntegrityViolationException;
    public Iterable<User> getAllUsers();
    public User getUserByEmail(String email);
    public User updateUserByEmail(User user);
}
