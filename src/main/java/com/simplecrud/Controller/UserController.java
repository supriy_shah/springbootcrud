package com.simplecrud.Controller;


import com.simplecrud.Entity.User;
import com.simplecrud.Repository.UserRepository;

import com.simplecrud.Service.IdentityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path="/demo")
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private IdentityService identityService;

    /*
    Simple GET implementation
     */
    @RequestMapping(method = RequestMethod.GET, path = "/addByGet")
    public @ResponseBody String addNewUser (@RequestParam String name,
                                            @RequestParam String email) {
       try{
           return identityService.createUser(new User(name, email));
       } catch (Exception e){
           return "Failed";
       }
    }

    /*
    Simple POST implementation
     */
    @RequestMapping(method = RequestMethod.POST, path = "/addByPost")
    public @ResponseBody String addUserByPost (@RequestBody User newUser){
        try {
            return identityService.createUser(newUser);
        }catch (Exception e){
            e.printStackTrace();
            return "Failed";
        }
    }

    /*
    Get all users
     */
    @RequestMapping(method = RequestMethod.GET, path = "/all")
    public @ResponseBody Iterable<User> getAllUsers() {
        return identityService.getAllUsers();
    }

    /*
    Different way of defining GET by including email ID in the API call. Not recommended.
     */
    @RequestMapping(method = RequestMethod.GET, path = "/getUser/{email}")
    public @ResponseBody User getUserByEmail (@PathVariable("email") String email){
        return identityService.getUserByEmail(email);
    }

    /*
    POST request to update user
     */
    @RequestMapping(method = RequestMethod.POST, path = "/updateUserByEmail")
    public @ResponseBody User updateUserByEmail (@RequestBody User updateUser){
        return identityService.updateUserByEmail(updateUser);
    }
}
